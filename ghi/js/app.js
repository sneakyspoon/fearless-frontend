function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow-sm" style="width: 1fr;">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <p class="card-text">${starts} - ${ends}</p>
            </div>
        </div>
    `
}

function badFetch() {
    return `
        <div class="alert alert-secondary" role="alert">
        Oh dear, there are no conferences to show.
        </div>
    `
}

function ohNo() {
    return `
        <div class="alert alert-danger" role="alert">
        Whoops, we've encountered an error.
        </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences'
    try {
        const response = await fetch(url)
        if (!response.ok) {
            badFetch()
        } else {
            const data = await response.json()
            const card = document.querySelectorAll('.col')
            let i = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const name = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const starts = new Date(details.conference.starts).toLocaleDateString()
                    const ends = new Date(details.conference.ends).toLocaleDateString()
                    const location = details.conference.location.name
                    const html = createCard(name, description, pictureUrl, starts, ends, location)
                    card[i].innerHTML += html
                    i++
                }
            }
        }
    } catch (e) {
        console.error(e);
        ohNo()
    }
})
