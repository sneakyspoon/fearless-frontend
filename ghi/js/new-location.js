window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = 'http://localhost:8000/api/states'

    try {
        const response = await fetch(statesUrl)

        if (!response.ok) {
            // do something when error
        } else {
            const data = await response.json()

            const selectTag = document.getElementById('state')

            for (let state of data) {
                const option = document.createElement("option")
                option.value = state.abbreviation
                option.innerHTML = state.name
                selectTag.appendChild(option)
            }
            console.log(data)
        }
    } catch (e) {
        // do something when error
    }
})
