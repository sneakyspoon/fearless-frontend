import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';


function App(props) {
  const [ attendees, setAttendees ] = useState([])
  const [ conferences, setConferences ] = useState([])

  async function getAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');
    if (response.ok) {
      const { attendees } = await response.json();
      setAttendees(attendees)
    } else {
      console.error('an error occurred fetching the data')
    }
  }

  async function getConferences() {
    const response = await fetch('http://localhost:8000/api/conferences/');
    if (response.ok) {
      const { conferences } = await response.json();
      setConferences(conferences);
    }
  }

  useEffect(() => {
    getAttendees()
    getConferences();
  }, [])

  if(attendees === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage conferences={conferences} />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm getConferences={getConferences} />} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList attendees={attendees} />} />
            <Route path="new" element={<AttendConferenceForm getAttendees={getAttendees} conferences={conferences} />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm conferences={conferences} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
