import React, { useEffect, useState } from 'react';


function ConferenceForm({ getConferences }){
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
        console.log(value);
    }

    const [starts, setStarts] = useState('');
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [max_presentations, setPresentations] = useState('');
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }

    const [max_attendees, setAttendees] = useState('');
    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);

        }
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;
        console.log(data);


        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
            getConferences()
          }
      }

      useEffect(() => {
        fetchData();
      }, []);
return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartsChange} required type="date" id="starts" name="starts" className="form-control" value={starts} />
              <label htmlFor="start">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndsChange} required type="date" id="ends" name="ends" className="form-control" value={ends} />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">Description</label>
              <textarea onChange={handleDescriptionChange} className="form-control" id="description" name="description" rows="3" value={description} ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePresentationsChange} required type="number" id="presentations" name="max_presentations" className="form-control" value={max_presentations} />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAttendeesChange} required type="number" id="attendees" name="max_attendees" className="form-control" value={max_attendees} />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select" >
                <option value="">Choose a location</option>
              {locations.map(location => {
                  return (
                    <option key={location.name} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
)
}

export default ConferenceForm;
